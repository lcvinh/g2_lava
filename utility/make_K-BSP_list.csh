#!/bin/sh -f
#Note: This program run on Linux
#Function: Make file K-BSP_list.csv from Test Program
#How_to_run: 
#Writen: RVC/AnhTran"
#Version: 0.1 First created

echo ""
echo "Start making K-BSP_list.csv for `pwd`"
rm -rf K-BSP_list.csv
echo "K-BSP,,,," > K-BSP_list.csv
echo "This is the environment for the TPREL_ENV1_ENVVER1.,This is the environment for the TPREL_ENV1_ENVVER1.,,," >> K-BSP_list.csv
echo "01 version,01 version,,," >> K-BSP_list.csv
echo "This is a test of LinuxBSP,This is a test of LinuxBSP,,," >> K-BSP_list.csv

ls -l | egrep BSP | egrep -v tgz | egrep "^d" | awk '{print $NF}' > list_dir
module_ID_list=`cat list_dir | awk -F'_' '{print $2}' | sort -u | egrep -v 004`
for module_ID in `echo $module_ID_list`
do
	#echo $module_ID	
	first_column=`echo $module_ID  | sed -e 's/^0//g' -e 's/^0//g'`
	module=`ls ../../../ | egrep Auto | egrep _D${module_ID}_ | awk -F'_' '{print $7}' | sed -n '1p'`
	echo "${module_ID}_$module"
	echo "${first_column},,,This is test of $module,This is test of $module" >> K-BSP_list.csv
	for second_column in `cat list_dir | egrep K-BSP_${module_ID} | awk -F'_' '{print $3}' | sort -u`
	do
		second_column_out=`echo $second_column | sed -e 's/^0//g' -e 's/^0//g'`
		first_dir=`ls -l | egrep "K-BSP_${module_ID}_${second_column}" | egrep "^d" | awk '{print $NF}' | sed -n '1 p'`
		big_feature=`egrep "Feature:" $first_dir/K_BSP_TP.c | awk -F'Feature: ' '{print $NF}'`
		echo ",${second_column_out},,$big_feature,$big_feature" >> K-BSP_list.csv
		for num_module in `egrep "K-BSP_${module_ID}_${second_column}" list_dir | awk -F'_' '{print $NF}'`
		do
			num_module_out=`echo $num_module | sed -e 's/^0//g' -e 's/^0//g'`
			#Refer Feature: & Details_description: in file K_BSP_TP.c
			Feature=`egrep "Feature:" K-BSP_${module_ID}_${second_column}_${num_module}/K_BSP_TP.c | awk -F'Feature:' '{print $NF}' | sed -e 's/^ //g'`
			Description=`egrep "Details_description:" K-BSP_${module_ID}_${second_column}_${num_module}/K_BSP_TP.c | awk -F'Details_description:' '{print $NF}' | sed -e 's/^ //g'`
			echo ",,${num_module_out},${Description},${Description}" >> K-BSP_list.csv
		done
	done
done
	rm -rf list_dir
	echo "Finish. Output: K-BSP_list.csv"
