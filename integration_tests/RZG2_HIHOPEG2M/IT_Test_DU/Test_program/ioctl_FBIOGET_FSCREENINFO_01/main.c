/*
* Project: CIP_LAVA_IT_TEST
* Test ID: ioctl_FBIOGET_FSCREENINFO_01
* Feature: Checking ioctl_FBIOGET_FSCREENINFO system call
* Sequence: open() -> ioctl()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_FBIOGET_FSCREENINFO to get fixed screen information. Expected result = OK
*/

#include <stdlib.h>
#include <stdio.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/

#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct fb_fix_screeninfo finfo;
	
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = open("/dev/fb0", O_RDWR | O_NOCTTY);	/* fb0 is the framebuffer */
												/* O_RDWR, O_RDONLY or O_WRONLY - Read/Write access to serial port */
												/* O_NOCTTY - No terminal will control the process */
	/* Get fixed screen information */
	result = ioctl(fd, FBIOGET_FSCREENINFO, &finfo);
	
	/* Check return value of sequence */
	if(result == 0) {
		printf ("OK\n");
	}
	else if(result == -1) {
		printf ("NG\n");
	}
	else{
		printf ("Unknown_result\n");
	}

	/* Close FB device */
	close(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

