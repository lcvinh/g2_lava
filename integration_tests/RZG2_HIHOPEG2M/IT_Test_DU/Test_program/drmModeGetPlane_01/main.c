/*
* Project: CIP_LAVA_IT_TEST
* Test ID: drmModeGetPlane_01
* Feature: Checking drmModeGetPlane system call
* Sequence: drmOpen() -> drmModeGetPlane()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call drmModeGetPlane to acquire a specific plane ID info from plane resources. Expected result = OK
*/

#include <stdio.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <libdrm/drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
drmModePlaneRes	*plane_resources;
drmModePlane	*plane;
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	int i;
	
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);
	
	/* Get Plane resources of the drm device */
	plane_resources = drmModeGetPlaneResources(fd);

	for (i = 0; i < plane_resources->count_planes; i++) {
		/* Valid plane ID is in [14-19] if we print out plane_resources->planes[i] value */
		plane = drmModeGetPlane(fd, plane_resources->planes[i]); 
		if (!plane) {
			result = -1;
			break;
		}
		else {
			result = 1;
			drmModeFreePlane(plane);
		}
	}
	
	/* Check return value of sequence */
	if(result == -1) {
		printf ("NG\n");
	}
	else if (result == 1) {
		printf ("OK\n");
	}
	else {
		printf ("Unknown_result\n");
	}

	/* Free Plane resources */
	drmModeFreePlaneResources(plane_resources);
	/* Close FB device */
	drmClose(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

