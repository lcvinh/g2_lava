/*
* Project: CIP_LAVA_IT_TEST
* Test ID: mmap_01
* Feature: Checking mmap system call
* Sequence: open() -> ioctl() -> mmap()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call mmap to map frame buffer into the user space. Expected result = OK
*/

#include <stdlib.h>
#include <stdio.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>	/* File Control Definitions				*/
#include <unistd.h>	/* UNIX Standard Definitions				*/
#include <errno.h>	/* ERROR Number Definitions				*/

#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	struct fb_var_screeninfo vinfo;
	size_t buffersize;
	char *result = NULL;
	
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = open("/dev/fb0", O_RDWR | O_NOCTTY);	/* fb0 is the framebuffer */
												/* O_RDWR, O_RDONLY or O_WRONLY - Read/Write access to serial port */
												/* O_NOCTTY - No terminal will control the process */
	
	/* Get variable screen information */
	ioctl(fd, FBIOGET_VSCREENINFO, &vinfo);

	/* Figure out the size of the buffer to be mapped in bytes */
	buffersize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;

	/* Map the device to memory */
	result = mmap(0, buffersize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	/* Check return value of sequence */
	if((result == MAP_FAILED) || (result == NULL)) {
		printf ("NG\n");
	}
	else{
		printf ("OK\n");
		munmap(result, buffersize);
	}

	/* Close FB device */
	close(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

