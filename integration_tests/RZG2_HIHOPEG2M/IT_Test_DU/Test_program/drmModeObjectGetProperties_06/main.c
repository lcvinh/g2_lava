/*
* Project: CIP_LAVA_IT_TEST
* Test ID: drmModeObjectGetProperties_06
* Feature: Checking drmModeObjectGetProperties system call
* Sequence: drmOpen() -> drmModeObjectGetProperties()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M
* Details_description: Condition: Call drmModeObjectGetProperties to acquire CRTC properties where CRTC ID is 69. Expected result = OK
*/

#include <stdio.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <libdrm/drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
drmModeObjectProperties	*result;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);
	
	/* Get properties of the target object */
	result = drmModeObjectGetProperties(fd, 69, DRM_MODE_OBJECT_CRTC);

	/* Check return value of sequence */
	if(!result) {
		printf ("NG\n");
	}
	else {
		printf ("OK\n");
		drmModeFreeObjectProperties(result);
	}

	/* Close FB device */
	drmClose(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

