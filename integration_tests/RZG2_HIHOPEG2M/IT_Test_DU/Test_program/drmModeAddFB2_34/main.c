/*
* Project: CIP_LAVA_IT_TEST
* Test ID: drmModeAddFB2_34
* Feature: Checking drmModeAddFB2 system call
* Sequence: drmOpen() -> kms_create() -> kms_bo_create() -> kms_bo_get_prop() -> kms_bo_map() -> kms_bo_unmap() -> drmModeAddFB2()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call drmModeAddFB2 to create Frame buffer Object with supported width size 1920; height size 1080; and pixel format DRM_FORMAT_YVU444. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <xf86drm.h>
#include <drm_fourcc.h>
#include <xf86drmMode.h>
#include <drm_mode.h>
#include <libkms.h>
#include <sys/errno.h>
#include <signal.h>

/* Declare global variable */
int	result = -1;

/* Declare signal handler */
void segfault_sigaction(int signal, siginfo_t *si, void *arg);

unsigned attributes[] = {
	KMS_WIDTH, 0,
	KMS_HEIGHT, 0,
	KMS_BO_TYPE, KMS_BO_TYPE_SCANOUT_X8R8G8B8,
	KMS_TERMINATE_PROP_LIST,
};

uint32_t pitches[4], handles[4], offsets[4];

int main()
{
	/* Declare local variable */
	int fd;
	struct kms_driver *kms_driver;
	struct kms_bo *bo;
	void *mapped_bo;
	uint32_t pixel_format, width, height, virtual_height;
	uint32_t fb_id;
	
	/* This block of code will catch segmentation fault error */
	struct sigaction sa;

	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	
	/* assign exception handler */
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;

	sigaction(SIGSEGV, &sa, NULL);
	
	/* Initialize variable and assign value for variable */
	width = 1920;
	height = 1080;
	pixel_format = DRM_FORMAT_YVU444;
	
	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);

	/* Create kms driver */
	kms_create(fd, &kms_driver);

	/* Create buffer object */
	switch (pixel_format) {
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
		virtual_height = height * 3 / 2;
		break;
	case DRM_FORMAT_NV16:
	case DRM_FORMAT_NV61:
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		virtual_height = height * 2;
		break;
	case DRM_FORMAT_YUV422:
	case DRM_FORMAT_YVU422:
	case DRM_FORMAT_YUV444:
	case DRM_FORMAT_YVU444:
		virtual_height = height * 3;
		break;
	default:
		virtual_height = height;
		break;
	}
	attributes[1] = width;
	attributes[3] = virtual_height;
	kms_bo_create(kms_driver, attributes, &bo);

	/* Get buffer object pitch/handle */
	kms_bo_get_prop(bo, KMS_PITCH, &pitches[0]);
	
	/* Map buffer object info user space */
	kms_bo_map(bo, &mapped_bo);
	
	/* Fill the buffer object according to the pixel format */
	switch (pixel_format) {
	case DRM_FORMAT_UYVY:
	case DRM_FORMAT_YUYV:
	case DRM_FORMAT_ARGB1555:
	case DRM_FORMAT_XRGB1555:
	case DRM_FORMAT_RGB565:
	case DRM_FORMAT_ARGB8888:
	case DRM_FORMAT_XRGB8888:
	case DRM_FORMAT_ARGB2101010:
	case DRM_FORMAT_BGR565:
	case DRM_FORMAT_RGB332:
	case DRM_FORMAT_ARGB4444:
	case DRM_FORMAT_XRGB4444:
	case DRM_FORMAT_BGR888:
	case DRM_FORMAT_RGB888:
	case DRM_FORMAT_BGRA8888:
	case DRM_FORMAT_BGRX8888:
	case DRM_FORMAT_YVYU:
		offsets[0] = 0;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[0]);
		kms_bo_get_prop(bo, KMS_PITCH, &pitches[0]);
		
		/* Fill the buffer area */
		memset(mapped_bo, 0x77, pitches[0] * height);
		break;
	case DRM_FORMAT_NV12:
	case DRM_FORMAT_NV21:
	case DRM_FORMAT_NV16:
	case DRM_FORMAT_NV61:
		offsets[0] = 0;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[0]);
		kms_bo_get_prop(bo, KMS_PITCH, &pitches[0]);
		pitches[1] = pitches[0];
		offsets[1] = pitches[0] * height;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[1]);

		/* Fill the buffer area */
		memset(mapped_bo, 0x77, pitches[0] * virtual_height);
		break;
	case DRM_FORMAT_YUV420:
	case DRM_FORMAT_YVU420:
		offsets[0] = 0;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[0]);
		kms_bo_get_prop(bo, KMS_PITCH, &pitches[0]);
		pitches[1] = pitches[0] / 2;
		offsets[1] = pitches[0] * height;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[1]);
		pitches[2] = pitches[1];
		offsets[2] = offsets[1] + pitches[1] * height/2;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[2]);

		/* Fill the buffer area */
		memset(mapped_bo, 0x77, pitches[0] * virtual_height);
		break;
	case DRM_FORMAT_YUV422:
	case DRM_FORMAT_YVU422:
		offsets[0] = 0;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[0]);
		kms_bo_get_prop(bo, KMS_PITCH, &pitches[0]);
		pitches[1] = pitches[0] / 2;
		offsets[1] = pitches[0] * height;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[1]);
		pitches[2] = pitches[1];
		offsets[2] = offsets[1] + pitches[1] * height;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[2]);

		/* Fill the buffer area */
		memset(mapped_bo, 0x77, pitches[0] * virtual_height);
		break;
	case DRM_FORMAT_YUV444:
	case DRM_FORMAT_YVU444:
		offsets[0] = 0;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[0]);
		kms_bo_get_prop(bo, KMS_PITCH, &pitches[0]);
		pitches[1] = pitches[0];
		offsets[1] = pitches[0] * height;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[1]);
		pitches[2] = pitches[1];
		offsets[2] = offsets[1] + pitches[1] * height;
		kms_bo_get_prop(bo, KMS_HANDLE, &handles[2]);

		/* Fill the buffer area */
		memset(mapped_bo, 0x77, pitches[0] * virtual_height);
		break;

	}
	/* Unmap buffer object */
	kms_bo_unmap(bo);

	result = drmModeAddFB2(fd, width, height, pixel_format, handles, pitches, offsets, &fb_id, 0);
	
	/* Check return value of sequence */
	if(result) {
		printf ("NG\n");
	}
	else{
		printf ("OK\n");
	}

	/* Destroy buffer object */
	kms_bo_destroy(&bo);

	/* Destroy kms driver */
	kms_destroy(&kms_driver);

	/* Close FB device */
	drmClose(fd);
	sleep(3);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SEG_FAULT\n");
	exit(0);
}
