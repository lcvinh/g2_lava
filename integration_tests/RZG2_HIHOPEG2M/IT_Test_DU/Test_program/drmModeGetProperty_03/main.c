/*
* Project: CIP_LAVA_IT_TEST
* Test ID: drmModeGetProperty_03
* Feature: Checking drmModeGetProperty system call
* Sequence: drmOpen() -> drmModeObjectGetProperties() -> drmModeGetProperty()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M
* Details_description: Condition: Call drmModeGetProperty to acquire all HDMIA (ID = 58) Connector properties info from Connector Property Object . Expected result = OK
*/
#include <stdio.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <libdrm/drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
drmModeObjectProperties	*props;
drmModePropertyPtr	prop;
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	int i;
	
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);
	
	/* Get properties of the target object */
	props = drmModeObjectGetProperties(fd, 72, DRM_MODE_OBJECT_CONNECTOR); /* Connector ID is 													72 */

	if(!props){
		result = 0;
		goto Exit;
	}

	/* Get property info of the target object's properties */
	for (i = 0; i < props->count_props; i++){
		prop = drmModeGetProperty(fd, props->props[i]);
		if (!prop) {
			result = -1;
			break;
		}
		else {
			result = 1;
			drmModeFreeProperty(prop);
		}
	}
	
	/* Check return value of sequence */
Exit:	if(result == -1) {
		printf ("NG\n");
	}
	else if (result == 1) {
		printf ("OK\n");
	}
	else if (result == 0) {
		printf ("Error\n");
	}	
	else {
		printf ("Unknown_result\n");
	}

	drmModeFreeObjectProperties(props);
	/* Close FB device */
	drmClose(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

