/*
* Project: CIP_LAVA_IT_TEST
* Test ID: drmModeGetPlaneResources_02
* Feature: Checking drmModeGetPlaneResources system call
* Sequence: drmOpen() -> drmModeGetPlaneResources()
* Testing level: system call
* Test-case type: Abnormal
* Expected result: NG
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call drmModeGetPlaneResources to acquire plane resources info of invalid DRM device file descriptor. Expected result = NG
*/

#include <stdio.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <libdrm/drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
drmModePlaneRes	*result;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int fd;
	int i; 
	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);
	
	/* Get Plane resources of the drm device */
	result = drmModeGetPlaneResources(-1);

	/* Check return value of sequence */
	if(!result) {
		printf ("NG\n");
	}
	else{
		printf ("OK\n");
		drmModeFreePlaneResources(result);
	}

	/* Close FB device */
	drmClose(fd);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

