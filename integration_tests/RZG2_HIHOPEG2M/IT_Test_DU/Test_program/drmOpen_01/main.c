/*
* Project: CIP_LAVA_IT_TEST
* Test ID: drmOpen_01
* Feature: Checking drmOpen system call
* Sequence: drmOpen()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call drmOpen to open the DRM device. Expected result = OK
*/

#include <stdio.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <libdrm/drm.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */

	/* Initialize variable and assign value for variable */

	/* Call API or system call follow describe in PCL */
	result = drmOpen("rcar-du", NULL);
	
	/* Check return value of sequence */
	if(result >= 0) {
		printf ("OK\n");
	}
	else if(result == -1) {
		printf ("NG\n");
	}
	else{
		printf ("Unknown_result\n");
	}

	/* Close FB device */
	if(result >= 0)
		drmClose(result);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

