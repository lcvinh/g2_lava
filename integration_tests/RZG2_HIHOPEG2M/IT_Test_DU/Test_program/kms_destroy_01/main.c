/*
* Project: CIP_LAVA_IT_TEST
* Test ID: kms_destroy_01
* Feature: Checking kms_destroy system call
* Sequence: drmOpen() -> kms_create() -> kms_destroy()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call kms_destroy to destroy kms driver. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <xf86drm.h>
#include <drm_fourcc.h>
#include <xf86drmMode.h>
#include <drm_mode.h>
#include <libkms.h>
#include <sys/errno.h>
#include <signal.h>

/* Declare global variable */
struct kms_driver *kms_driver;
int	result = -1;

/* Declare signal handler */
void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main()
{
	/* Declare local variable */
	int fd;

	/* Initialize variable and assign value for variable */

	/* This block of code will catch segmentation fault error */
	struct sigaction sa;

	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);

	/* assign exception handler */
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;

	sigaction(SIGSEGV, &sa, NULL);

	/* Call API or system call follow describe in PCL */
	fd = drmOpen("rcar-du", NULL);

	/* Create kms driver */
	kms_create(fd, &kms_driver);

	/* Destroy kms driver */
	result = kms_destroy(&kms_driver);

	/* Check return value of sequence */
	if(result) {
		printf ("NG\n");
		kms_destroy(&kms_driver);
	}
	else{
		printf ("OK\n");
	}

	/* Close FB device */
	drmClose(fd);

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SEGFAULT\n");
	exit(0);
}
