/*
* Project: CIP_LAVA_IT_TEST
* Test ID: ssi_playback_mmap_noninterleave_05
* Feature: Checking ALSA API ssi_playback_mmap_noninterleave of SSI
* Sequence: Call ssi_playback_mmap_noninterleave
* Testing level: ALSA API
* Test-case type: NORMAL
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ssi_playback_mmap_noninterleave
*/
#include <alsa/asoundlib.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main() {
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int result;
	int dir = 0;
	int rate = 44100;
	char buffer[1];
	void* bufs[1] = {NULL};
	snd_pcm_t *handle;
	snd_pcm_hw_params_t *hw_params;

	snd_pcm_open(&handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
	snd_pcm_hw_params_malloc(&hw_params);
	snd_pcm_hw_params_any(handle, hw_params);
	snd_pcm_hw_params_set_access(handle, hw_params, SND_PCM_ACCESS_MMAP_NONINTERLEAVED);

	if((result = snd_pcm_hw_params_set_format(handle, hw_params, SND_PCM_FORMAT_S16_BE)) < 0) {
		printf("SET_FORMAT_ERROR\n");
		return 0;
	}

	snd_pcm_hw_params_set_rate_near(handle, hw_params, &rate, &dir);
	snd_pcm_hw_params_set_channels(handle, hw_params, 1);

	if((result = snd_pcm_hw_params(handle, hw_params)) < 0) {
		printf("HW_PARAMS_ERROR\n");
		return 0;
	}
	snd_pcm_hw_params_free(hw_params);

	buffer[0] = random() & 0xff;
	bufs[0] = &buffer;
	result = snd_pcm_mmap_writen(handle, bufs, sizeof(buffer));
	snd_pcm_close(handle);

	if(result > 0) 
		printf("OK\n");	
	else
		printf("ERROR\n");
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

