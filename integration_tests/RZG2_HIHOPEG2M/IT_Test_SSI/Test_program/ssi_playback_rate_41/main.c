/*
* Project: CIP_LAVA_IT_TEST
* Test ID: ssi_playback_rate_41
* Feature: Checking ALSA API ssi_playback_rate of SSI
* Sequence: Call ssi_playback_rate
* Testing level: ALSA API
* Test-case type: NORMAL
* Expected result: SET_PARAMS_ERROR
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ssi_playback_rate
*/
#include <alsa/asoundlib.h>

#include <signal.h>
#include <string.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

unsigned char buffer[1];

int main() {
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int result;
	unsigned int i;
	snd_pcm_t *handle;
	snd_pcm_sframes_t frames;

	snd_pcm_open(&handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
	if((result = snd_pcm_set_params(handle,
					SND_PCM_FORMAT_S16_LE,
					SND_PCM_ACCESS_RW_INTERLEAVED,
					2,
					33000,
					1,
					600000)) < 0) {
		printf("SET_PARAMS_ERROR\n");
		return 0;
	} 
	
	buffer[0] = random() & 0xff;
	result = snd_pcm_writei(handle, buffer, sizeof(buffer));
	
	if(result > 0) 
		printf("OK\n");	
	else
		printf("ERROR\n");
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

