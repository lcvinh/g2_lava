#!/bin/sh                                                                                                                                                                                                    
for device in `find /sys/class/net/* -name "can*" | awk -F '\/' '{print $NF}'`; do
ip link set $device up type can bitrate 33333
if [ $device = "can0" ]
then
devmem 0xE6C30840 h 0x090D
devmem 0xE6C30844 w 0xC07C0100
devmem 0xE6C30840 h 0x0A0D
devmem 0xE6C30858 b 0x07
devmem 0xE6C30840 h 0x080D
devmem 0xE6C30848 b 0x81
devmem 0xE6C3084A b 0x81
elif [ $device = "can1" ]
then
devmem 0xE6C38840 h 0x090D
devmem 0xE6C38844 w 0xC07C0100
devmem 0xE6C38840 h 0x0A0D
devmem 0xE6C38858 b 0x07
devmem 0xE6C38840 h 0x080D
devmem 0xE6C38848 b 0x81
devmem 0xE6C3884A b 0x81
else
none
fi
done
