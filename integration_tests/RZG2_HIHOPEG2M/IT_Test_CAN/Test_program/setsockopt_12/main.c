/*
* Project: CIP_LAVA_IT_TEST
* Test ID: setsockopt_12
* Feature: Checking setsockopt system call
* Sequence: socket();setsockopt()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call setsockopt after call socket with input PF_CAN; SOCK_RAW; CAN_RAW; SOL_CAN_RAW; CAN_RAW_ERR_FILTER. Expected result = OK
*/
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include <linux/sockios.h>
#include <fcntl.h>
#include <errno.h> /*to print string error*/
#include <linux/can/error.h>


#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	//Call set of functions under test
	fd = socket(PF_CAN, SOCK_RAW, CAN_RAW);

        can_err_mask_t err_mask = (CAN_ERR_TX_TIMEOUT | 
                                   CAN_ERR_LOSTARB | 
                                   CAN_ERR_PROT |                               
                                   CAN_ERR_TRX |
                                   CAN_ERR_ACK | 
                                   CAN_ERR_BUSOFF | CAN_ERR_BUSERROR | 
                                   CAN_ERR_RESTARTED );
         
	result = setsockopt(fd,SOL_CAN_RAW,CAN_RAW_ERR_FILTER,&err_mask,sizeof(err_mask));
	//printf("Error %s\n",strerror(errno));
	if( result < 0){
		printf("NG\n");
	}
	else{
		printf("OK\n");
	}
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

