/*
* Project: CIP_LAVA_IT_TEST
* Test ID: ioctl_SIOCGIFTXQLEN_04
* Feature: Checking ioctl_SIOCGIFTXQLEN system call
* Sequence: socket();ioctl_SIOCGIFTXQLEN()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_SIOCGIFTXQLEN with device eth1 after socket with input PF_CAN; SOCK_DGRAM; CAN_BCM. Expected result = OK
*/
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include "get_vendor_ID_and_device.h"

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	struct ifreq	ifreq_dev;
	memset(&ifreq_dev, 0, sizeof(ifreq_dev));

	//Get eth* device of AVB from file ./board_config.txt
	char  *eth_dev = "can1";
	//eth_dev = get_ethernet_device("AVB_DEV");

	//Call set of functions under test
	fd = socket(PF_CAN, SOCK_DGRAM, CAN_BCM);
	strncpy(ifreq_dev.ifr_name, eth_dev, sizeof ifreq_dev.ifr_name);
	result = ioctl(fd, SIOCGIFTXQLEN, &ifreq_dev);
	//printf("ifreq_dev.ifr_qlen: %d\n",ifreq_dev.ifr_qlen);
	switch(result) {
	case 0:
		printf ("OK\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unkonw_result\n");
	};
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

