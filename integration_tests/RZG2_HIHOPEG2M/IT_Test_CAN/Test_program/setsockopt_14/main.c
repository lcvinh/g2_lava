/*
* Project: CIP_LAVA_IT_TEST
* Test ID: setsockopt_14
* Feature: Checking setsockopt system call
* Sequence: socket();setsockopt()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call setsockopt after call socket with input PF_CAN; SOCK_RAW; CAN_RAW; SOL_CAN_RAW; CAN_RAW_RECV_OWN_MSGS. Expected result = OK
*/
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include <linux/sockios.h>
#include <fcntl.h>
#include <errno.h> /*to print string error*/

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result, fd;
	int     recv_own_msgs = 1;
	socklen_t sock_len = sizeof(recv_own_msgs);
	//Call set of functions under test
	fd = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	result = setsockopt(fd,SOL_CAN_RAW,CAN_RAW_RECV_OWN_MSGS,&recv_own_msgs,sock_len);
	//printf("Error %s\n",strerror(errno));
	if( result < 0){
		printf("NG\n");
	}
	else{
		printf("OK\n");
	}
	close(fd);
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

