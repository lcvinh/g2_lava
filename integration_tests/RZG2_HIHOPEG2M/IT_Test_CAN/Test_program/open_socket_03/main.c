/*
* Project: CIP_LAVA_IT_TEST
* Test ID: open_socket_03
* Feature: Checking open_socket system call
* Sequence: socket()
* Testing level: system call
* Test-case type: Normal
* Expected result: NG
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call open_socket with input PF_CAN; SOCK_DGRAM; CAN_RAW. Expected result = NG
*/
// Declare library
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main (void)
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int	result;

	//Call set of functions under test
	result = socket(PF_CAN, SOCK_DGRAM, CAN_RAW);
	if(result > 0){
		printf ("OK\n");
		close(result);
	}
	else{
		printf ("NG\n");
	}

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

