/*
* Project: CIP_LAVA_IT_TEST
* Test ID: bcm_txsend_01
* Feature: Checking bcm_txsend system call
* Sequence: head opcode: TX_SEND
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: head opcode: TX_SEND
*/
// Declare library
#include <stdio.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <linux/can.h>
#include <linux/can/bcm.h>
#include <net/if.h>
#include <sys/ioctl.h>

#include <stdlib.h>
#include <signal.h>

#define max(a,b) (a > b ? a : b)
#define U64_DATA(p) (*(unsigned long long*)(p)->data)

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int fd, i, nbytes;
	struct sockaddr_can addr;
	struct ifreq ifr;
//	struct rxs rx;
//	struct can_frame frame; /*data structure for CAN frames*/
//        int loopback = 1 ;
	struct {
		struct bcm_msg_head msg_head;
		struct can_frame frame;
	} msg;


	struct timeval tv;
	int result = -1;


	if ((fd = socket(PF_CAN, SOCK_DGRAM, CAN_BCM)) < 0) {
		perror("socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "can0");
	ioctl(fd, SIOCGIFINDEX, &ifr);
	addr.can_ifindex = ifr.ifr_ifindex;
	addr.can_family = PF_CAN;

	if (connect(fd, (struct sockaddr *)&addr,sizeof(addr))) {
		perror("connect");
		return 1;
	}

	msg.msg_head.opcode  = TX_SEND;
	msg.msg_head.can_id  = 0x42;
	msg.msg_head.flags   = 0;
	msg.msg_head.nframes = 1;
	msg.msg_head.count = 0;
	msg.msg_head.ival1.tv_sec = 0;
	msg.msg_head.ival1.tv_usec = 0;
	msg.msg_head.ival2.tv_sec = 0;
	msg.msg_head.ival2.tv_usec = 0;
	msg.frame.can_id    = 0x42;
	msg.frame.can_dlc   = 6;
	msg.frame.data[0] = 0xA0;
	msg.frame.data[1] = 0xA1;
	msg.frame.data[2] = 0xA2;
	msg.frame.data[3] = 0xA3;
	msg.frame.data[4] = 0xA4;
	msg.frame.data[5] = 0xA5;

	result = write(fd, &msg, sizeof(msg));

	if( result < 0){
		printf("NG\n");
	}
	else{
		printf("OK\n");
	}
	close(fd);
	 
return 0;
}
void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

