/*
* Project: CIP_LAVA_IT_TEST
* Test ID: LOOPBACK_01
* Feature: Checking LOOPBACK system call
* Sequence: server();client()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call LOOPBACK by calling 2 thread server and client with input PF_CAN; SOCK_RAW; CAN_RAW to check loopback mode. Expected result = OK
*/
// Declare library
#include <stdio.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <sys/ioctl.h>

#include <stdlib.h>
#include <signal.h>

#define max(a,b) (a > b ? a : b)

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

struct rxs {
	int s;
	int t;
};

int test_sockets(int fd, struct can_frame frame)
{
	fd_set rdfs;
	struct timeval tv;
	int have_rx = 1;
	int ret = -1;

	if (write(fd, &frame, sizeof(frame)) < 0) {
		perror("write");
		exit(1);
	}
//	printf("frame.can_dlc : %x \n",frame.can_dlc);
//	for (int i = 0 ; i <= frame.can_dlc; i++) {
//	printf ("frame.data[%d]: %d \n ",i, frame.data[i] );
//	}

	while (have_rx) {

		FD_ZERO(&rdfs);
		FD_SET(fd, &rdfs);
		tv.tv_sec = 0;
		tv.tv_usec = 50000; /* 50ms timeout */
		have_rx = 0;
		int check_sel ;
		check_sel = select(fd + 1, &rdfs, NULL, NULL, &tv);
		if (check_sel < 0) {
			perror("select");
			exit(1);
		}
		if (FD_ISSET(fd, &rdfs)) {

			have_rx = 1;
			ret = read(fd, &frame, sizeof(struct can_frame));
		}
	}

	/* timeout */

	return ret;
}


int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int fd;
	struct sockaddr_can addr;
	struct ifreq ifr;
	struct rxs rx;
	struct can_frame frame; /*data structure for CAN frames*/
        int loopback = 1 ;

	int result = -1;


	if ((fd = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "can0");
	if (ioctl(fd, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		return 1;
	}
	addr.can_ifindex = ifr.ifr_ifindex;
	addr.can_family = PF_CAN;

	if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}
	setsockopt(fd, SOL_CAN_RAW, CAN_RAW_LOOPBACK,
		   &loopback, sizeof(loopback));
	frame.can_id = 0x432;
	frame.can_dlc = 0;
//	frame.can_dlc = 2;
//	frame.data[0] = 0x10;
//	frame.data[1] = 0xA0;
	result = test_sockets(fd, frame);
//	printf("result = %d \n",result);
	if (result < 0) {
		printf("NG \n");
		exit(1);
	}
	else {
		printf("OK \n");
        }

	close(fd);

	return 0;
}
void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

