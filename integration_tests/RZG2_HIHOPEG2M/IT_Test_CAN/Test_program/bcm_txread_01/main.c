/*
* Project: CIP_LAVA_IT_TEST
* Test ID: bcm_txread_01
* Feature: Checking bcm_txread system call
* Sequence: head opcode: TX_READ
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: head opcode: TX_READ
*/
// Declare library
#include <stdio.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <linux/can.h>
#include <linux/can/bcm.h>
#include <net/if.h>
#include <sys/ioctl.h>

#include <stdlib.h>
#include <signal.h>

#define U64_DATA(p) (*(unsigned long long*)(p)->data)

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	int fd, i;
	struct sockaddr_can addr;
	struct ifreq ifr;

	struct {
		struct bcm_msg_head msg_head;
		struct can_frame frame[4];
	} msg;


	int result = -1;


	if ((fd = socket(PF_CAN, SOCK_DGRAM, CAN_BCM)) < 0) {
		perror("socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "can0");
	if (ioctl(fd, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		return 1;
	}
	addr.can_ifindex = ifr.ifr_ifindex;
	addr.can_family = PF_CAN;

	if (connect(fd, (struct sockaddr *)&addr,sizeof(addr))) {
		perror("connect");
		return 1;
	}

	msg.msg_head.opcode  = TX_SETUP;
	msg.msg_head.can_id  = 0x42;
	msg.msg_head.flags   = SETTIMER|STARTTIMER|TX_CP_CAN_ID;
	msg.msg_head.nframes = 1;
	msg.msg_head.count = 0;
	msg.msg_head.ival1.tv_sec = 0;
	msg.msg_head.ival1.tv_usec = 0;
	msg.msg_head.ival2.tv_sec = 0;
	msg.msg_head.ival2.tv_usec = 0;
	msg.frame[0].can_id    = 0xAA;
	msg.frame[0].can_dlc   = 8;
	U64_DATA(&msg.frame[0]) = (__u64) 0xcafecafecafecafeULL;

	if (write(fd, &msg, sizeof(msg)) < 0)
		perror("write");

	msg.msg_head.opcode  = TX_READ;
	msg.msg_head.can_id  = 0x42;
	msg.msg_head.nframes = 0;

	if (write(fd, &msg, sizeof(msg)) < 0)
		perror("write");

	result = read(fd, &msg, sizeof(msg));
	//for (i = 0; i < result; i++)       
	//      printf(" %02x", ((unsigned char*)&msg)[i]);	

	if( result < 0){
		printf("NG\n");
	}
	else{
		printf("OK\n");
	}
	close(fd);

return 0;
}
void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

