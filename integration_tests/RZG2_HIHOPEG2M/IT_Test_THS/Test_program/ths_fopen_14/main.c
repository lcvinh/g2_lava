/*
* Project: CIP_LAVA_IT_TEST
* Test ID: ths_fopen_14
* Feature: Checking ths_fopen system call
* Sequence: 
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call Open device /sys/class/thermal/thermal_zone2/trip_point_0_type. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	FILE	*fd;
	int	result = -1;
	int 	size = 10;
	char	get_temp_value[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	fd = fopen("/sys/class/thermal/thermal_zone2/trip_point_0_type", "r");
//	printf("%d\n", fd);	//For debug only

	/* Check return value of sequence */
	if (fd == 0) {
		printf ("NG\n");
	}
	else if (fd > 0) {
		printf ("OK\n");
		fclose(fd);
	}
	else {
		printf ("Unknown_result\n");
	}
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

