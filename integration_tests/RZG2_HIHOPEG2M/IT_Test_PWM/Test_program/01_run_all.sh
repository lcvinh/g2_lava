#!/bin/bash
#Function: Run all Test Program 
#Author: AnhT 
#Version:
#Project: LAVA 
#Note: This tool run on Terminal of Board 

#=========================================================

CURPATH=`pwd`
for tc_dir in `ls -l | egrep "^d" | egrep "_" | awk '{print $NF}'`
do
	echo $tc_dir
	cd $CURPATH/$tc_dir
	if [ -f "./config.sh" ]
	then
		./config.sh
	fi

	./runtest.sh $tc_dir

	if [ -f "./restore" ]
	then
		./restore
	fi
	
done
