/*
* Project: CIP_LAVA_IT_TEST
* Test ID: pwm_enable_18
* Feature: Checking pwm_enable system call
* Sequence: pwm_export -> pwm_set_period() -> pwm_set_duty_cycle() -> pwm_enable()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call pwm_enable to start or stop PWM channel 0 operation (0: disable and 1: enable); chosen value is: 0. Expected result = OK
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define BUF_SIZE	64

/* Export PWM to user space */
int pwm_export(unsigned int pwm)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/pwm/pwmchip%d/export", pwm);

	fd = open(buf, O_WRONLY);
	if (fd < 0)
		return fd;

	if (write(fd, "0", 1) < 0)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Free PWM */
int pwm_unexport(unsigned int pwm)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/pwm/pwmchip%d/unexport", pwm);

	fd = open(buf, O_WRONLY);
	if (fd < 0)
		return fd;

	if (write(fd, "0", 1) < 0)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Set PWM Period */
int pwm_set_period(unsigned int pwm, char *period)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/pwm/pwmchip%d/pwm0/period", pwm);
	
	fd = open(buf, O_WRONLY);
	if (fd < 0)
		return fd;
	
	if (write(fd, period, 10) < 0)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Set PWM Duty cycle */
int pwm_set_duty_cycle(unsigned int pwm, char *duty_cycle)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/pwm/pwmchip%d/pwm0/duty_cycle", pwm);
	
	fd = open(buf, O_WRONLY);
	if (fd < 0)
		return fd;
	
	if (write(fd, duty_cycle, 10) < 0)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Enable PWM channel - 1: enable 0: disable */ 
int pwm_enable(unsigned int pwm, char *value)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/pwm/pwmchip%d/pwm0/enable", pwm);
	
	fd = open(buf, O_WRONLY);
	if (fd < 0)
		return fd;
	
	if (write(fd, value, 10) < 0)
		return -1;
	
	close(fd);
	
	return 0;
}

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int result = -1;
	int pwm = 0;
	
	/* Export the PWM into User Space */
	pwm_export(pwm);
	
	/* Reset PWM before setting new value */
	pwm_enable(pwm, "0");
	pwm_set_duty_cycle(pwm, "0");

	/* Set period value for PWM pin */
	pwm_set_period(pwm, "50000");
	
	/* Set duty cycle value for PWM pin */
	pwm_set_duty_cycle(pwm, "25000");

	/* Enable PWM channel - 1: enable 0: disable */
	result = pwm_enable(pwm, "0");
	/* Check return value of sequence */
	if (result == 0) {
		printf ("OK\n");
	}
	else if (result < 0) {
		printf ("NG\n");
	}
	else {
		printf ("Unknown_result\n");
	}
	
	/* Free the PWM */
	pwm_unexport(pwm);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

