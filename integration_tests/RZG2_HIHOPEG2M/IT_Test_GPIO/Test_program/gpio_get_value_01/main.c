/*
* Project: CIP_LAVA_IT_TEST
* Test ID: gpio_get_value_01
* Feature: Checking gpio_get_value system call
* Sequence: gpio_export -> gpio_set_dir() -> gpio_get_value()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call gpio_get_value to read the value (Low: 0 High:1) of GPIO pin GP5_05 in INPUT mode. Expected result = OK
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define BUF_SIZE	32

#include "gpio_pin_header_g2e.h"

typedef enum PIN_DIRECTION {
	INPUT_PIN = 0,
	OUTPUT_PIN = 1
} PIN_DIRECTION;

typedef enum PIN_VALUE {
	LOW = 0,
	HIGH = 1
} PIN_VALUE;

/* Export GPIO to user space */
int gpio_export(unsigned int gpio)
{
	int fd;
	char buf[BUF_SIZE];
	
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (fd < 0)
		return fd;

	sprintf(buf, "%d", gpio);
	
	if (write(fd, buf, strlen(buf)) == -1)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Free GPIO */
int gpio_unexport(unsigned int gpio)
{
	int fd;
	char buf[BUF_SIZE];
	
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (fd < 0)
		return fd;
	
	sprintf(buf, "%d", gpio);
	
	if (write(fd, buf, strlen(buf)) == -1)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Set input/ouput mode for the GPIO */
int gpio_set_dir(unsigned int gpio, PIN_DIRECTION dir_flag)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio);
	
	fd = open(buf, O_WRONLY);
	if (fd < 0)
		return fd;
	
	if (dir_flag == INPUT_PIN) {
		if (write(fd, "in", 3) < 0)
			return -1;
	}
	else {
		if (write(fd, "out", 4) < 0)
			return -1;
	}
	
	close(fd);
	
	return 0;
}

/* Read value from the GPIO pin */
int gpio_get_value(unsigned int gpio, PIN_VALUE *value)
{
	int fd, ret = 0;
	char buf[BUF_SIZE];
	char ch;
	
	sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
	
	fd = open(buf, O_RDONLY);
	if (fd < 0)
		return fd;
	
	if (!value)
		ret = -1;
	else {
		if(read(fd, &ch, 1) < 0)
			return -1;
		
		if (ch == '0') {
			*value = LOW;
		}
		else {
			*value = HIGH;
		}
	}
	close(fd);
	
	return ret;
}

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int	result = -1;
	int gpio = GP5_05;
	unsigned int pin_value;
	
	/* Export the GPIO into User Space */
	gpio_export(gpio);

	/* Set direction for GPIO pin */
	gpio_set_dir(gpio, INPUT_PIN);
	
	/* Read value from the GPIO pin */
	result = gpio_get_value(gpio, &pin_value);
	
	/* Check return value of sequence */
	if (result == 0) {
		printf ("OK\n");
	}
	else if (result < 0) {
		printf ("NG\n");
	}
	else {
		printf ("Unknown_result\n");
	}
	
	/* Free the GPIO */
	gpio_unexport(gpio);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

