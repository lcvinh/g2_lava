/*
* Project: CIP_LAVA_IT_TEST
* Test ID: gpio_set_dir_06
* Feature: Checking gpio_set_dir system call
* Sequence: gpio_export -> gpio_set_dir()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call gpio_set_dir to set the direction of GPIO pin GP5_19 to OUTPUT mode. Expected result = OK
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <signal.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

#define BUF_SIZE	32

#include "gpio_pin_header_g2e.h"


typedef enum PIN_DIRECTION {
	INPUT_PIN = 0,
	OUTPUT_PIN = 1
} PIN_DIRECTION;

/* Export GPIO to user space */
int gpio_export(unsigned int gpio)
{
	int fd;
	char buf[BUF_SIZE];
	
	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (fd < 0)
		return fd;

	sprintf(buf, "%d", gpio);
	
	if (write(fd, buf, strlen(buf)) == -1)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Free GPIO */
int gpio_unexport(unsigned int gpio)
{
	int fd;
	char buf[BUF_SIZE];
	
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (fd < 0)
		return fd;
	
	sprintf(buf, "%d", gpio);
	
	if (write(fd, buf, strlen(buf)) == -1)
		return -1;
	
	close(fd);
	
	return 0;
}

/* Set input/ouput mode for the GPIO */
int gpio_set_dir(unsigned int gpio, PIN_DIRECTION dir_flag)
{
	int fd;
	char buf[BUF_SIZE];
	
	sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio);
	
	fd = open(buf, O_WRONLY);
	if (fd < 0)
		return fd;
	
	if (dir_flag == INPUT_PIN) {
		if (write(fd, "in", 3) < 0)
			return -1;
	}
	else {
		if (write(fd, "out", 4) < 0)
			return -1;
	}
	
	close(fd);
	
	return 0;
}

int main(int argc, char *argv[])
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int	result = -1;
	int gpio = GP5_19;
	
	/* Export the GPIO into User Space */
	gpio_export(gpio);
	
	/* Set direction for GPIO pin */
	result = gpio_set_dir(gpio, OUTPUT_PIN);
	
	/* Check return value of sequence */
	if (result == 0) {
		printf ("OK\n");
	}
	else if (result < 0) {
		printf ("NG\n");
	}
	else {
		printf ("Unknown_result\n");
	}

	/* Free the GPIO */
	gpio_unexport(gpio);
	
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

