/*
* Project: CIP_LAVA_IT_TEST
* Test ID: pci_set_name_list_path_01
* Feature: Checking pci_set_name_list_path system call
* Sequence: pci_alloc();pci_init();pci_set_name_list_path()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: PCI card. Condition: Call API pci_set_name_list_path of pciutils library with File_name = FILE_NAME; Free_id_name = 1. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pci/pci.h>
#include "get_vendor_ID_and_device.h"

void segfault_sigaction(int signal, siginfo_t *si, void *arg);
const char *FILE_NAME = "cip";
int main(void)
{
	struct pci_access	*pci_acc;
	struct pci_dev		*device;
	char data[1024];
	int result;
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Get Vendor_ID of PCI Device from file ./board_config.txt
	int VENDOR_ID;
	VENDOR_ID = get_Vendor_ID_USB_PCI("PCI_ID");

	pci_acc = pci_alloc();	//Get pci_access
	//if (pci_acc->writeable == 0) {	//Expected: 0
	//	printf("OK\n");
	//}

	pci_init(pci_acc);	//Initialize PCI library
	
//	pci_scan_bus(pci_acc);	//Using pci_scan_bus() to check operation of pci_init()
//	for (device=pci_acc->devices; device; device=device->next)      /* Iterate over all devices */
//	{
//		if (device->vendor_id == VENDOR_ID){
//			break; /* detected PCIE card, access data done, break from "for" loop here*/
//		}
//	}	
	char *name;
	name = malloc(sizeof(char)*100);
	strncpy(name,FILE_NAME,sizeof(name));
	int to_be_freed = 1;
	pci_set_name_list_path(pci_acc,name,to_be_freed);
	if((strcmp(pci_acc->id_file_name,"cip") == 0) && (pci_acc->free_id_name == to_be_freed)){
		result  = 1;
	}
	else{
		result  = 0;
	}
	if(result == 1)	{
		 printf("OK\n");
	}
	else{
		printf("NG\n");
	}	
	pci_cleanup(pci_acc); //Close all

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");	//If call pci_scan_bus() without pci_init(), segfault will happen
	exit(0);
}
