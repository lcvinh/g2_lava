/*
* Project: CIP_LAVA_IT_TEST
* Test ID: ioctl_I2C_FUNCS_105
* Feature: Checking ioctl_I2C_FUNCS system call
* Sequence: open(); ioctl(I2C_FUNCS); close()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_I2C_FUNCS with device: /dev/i2c-4; mode: O_RDWR and call ioctl(I2C_FUNCS) to get functionality mask; then check function I2C_FUNC_SMBUS_WRITE_BLOCK_DATA and close(). Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int	fd;
	int	funcs;
	int	func_support = -1;

	/* Call API or system call follow describe in PCL */
	fd = open("/dev/i2c-4", O_RDWR);	/*** I2C CHANNEL ***/

	result = ioctl(fd, I2C_FUNCS, &funcs);			/*** I2C FUNCS ***/

	/* Check return value of sequence */
	switch(result) {
	case 0:
		func_support = funcs & I2C_FUNC_SMBUS_WRITE_BLOCK_DATA;
		if (func_support)
			printf ("OK\n");
		else
			printf ("NG_func_not_supported\n");
		break;
	case -1:
		printf ("NG\n");
		break;
	default:
		printf ("Unknown_result\n");
	};
	close(fd);						/*** I2C CLOSE ***/
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

