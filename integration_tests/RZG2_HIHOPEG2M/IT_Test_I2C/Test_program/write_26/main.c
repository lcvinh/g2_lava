/*
* Project: CIP_LAVA_IT_TEST
* Test ID: write_26
* Feature: Checking write system call
* Sequence: open(); ioctl(I2C_SLAVE_FORCE); write(); close()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call write with device: /dev/i2c-2; mode: O_RDWR and call ioctl(I2C_SLAVE_FORCE) to change slave address to OV4645_ADDR; write with byte count = 1 then close. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <errno.h>

#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;
int	diff_read_write = -1;

/*Input_slave_address*/
int CODEC_ADDR = 0x10;
int PMIC_ADDR  = 0x30;
int HDMI_ADDR  = 0x39;
int CLK_MPLIER_ADDR = 0x4F;
int VIDEO_RECV_ADDR = 0x70;
int CS2000_CP_ADDR      = 0x4F;
int PCA9654EDTR2G_ADDR  = 0x40;
int D9FGV0841AKILF_ADDR = 0x68;
int D5P49V5923B_ADDR    = 0x6A;
int OV4645_ADDR		= 0x3C;
int PCA9654_ADDR	= 0x20;
int VERSACLOCK_ADDR	= 0x6A;
int HD3SS_ADDR   	= 0x47;
int TDA19_ADDR          = 0x70;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int	fd;
	int	i;
	size_t	count = 1;
	__u8	buf[2] = {0x04, 0x0f};

	/* Call API or system call follow describe in PCL */
	fd = open("/dev/i2c-2", O_RDWR);	/*** I2C CHANNEL ***/

	ioctl(fd, I2C_SLAVE_FORCE, OV4645_ADDR);		/*** SLAVE ADDRESS ***/
	sleep(1);

	result = write(fd, buf, count);				/*** WRITE DATA ***/
	sleep(1);
	
	/* Check return value of sequence */
	if ((result >= 0) && (result == count))
		printf ("OK\n");
	else if (result == -1)
		printf ("NG\n");
	else
		printf ("Unknown_result\n");
	close(fd);						/*** I2C CLOSE ***/
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

