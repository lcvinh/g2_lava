/*
* Project: CIP_LAVA_IT_TEST
* Test ID: ioctl_I2C_RDWR_41
* Feature: Checking ioctl_I2C_RDWR system call
* Sequence: open(); ioctl(I2C_SLAVE_FORCE); ioctl(I2C_RDWR); close()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/VinhLuong (vinh.luong.gx@renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: HIHOPEG2M_HIHOPEG2N
* Details_description: Condition: Call ioctl_I2C_RDWR with device: /dev/i2c-4; mode: O_RDONLY and call ioctl(I2C_RDWR) with flag = 0; register address = 0x08; slave_addr = PCA9654_ADDR then close. Expected result = OK
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <errno.h>

#include <signal.h>
#include <string.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

/* Declare global variable */
int	result = -1;
int     STMPE811 = 0x44;
/*Input_slave_address*/
int CODEC_ADDR = 0x10;
int PMIC_ADDR  = 0x30;
int HDMI_ADDR  = 0x39;
int CLK_MPLIER_ADDR = 0x4F;
int VIDEO_RECV_ADDR = 0x70;
int CS2000_CP_ADDR      = 0x4F;
int PCA9654EDTR2G_ADDR  = 0x40;
int D9FGV0841AKILF_ADDR = 0x68;
int D5P49V5923B_ADDR    = 0x6A;
int OV4645_ADDR		= 0x3C;
int PCA9654_ADDR	= 0x20;
int VERSACLOCK_ADDR	= 0x6A;
int HD3SS_ADDR		= 0x47;
int TDA19_ADDR		= 0x70;

int main()
{
	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	/* Declare local variable */
	int	fd;
	int	i;
	struct	i2c_rdwr_ioctl_data msgset;
	struct	i2c_msg msg;
	__u8	reg_addr	= 0x08;
	__u8	data_byte_1	= 0x10;
	__u8	data_byte_2	= 0x20;
	__u8	buf[3];

	/* Call API or system call follow describe in PCL */
	fd = open("/dev/i2c-4", O_RDONLY);	/*** I2C CHANNEL ***/
	ioctl(fd, I2C_SLAVE_FORCE, PCA9654_ADDR);		/*** SLAVE ADDRESS ***/

	buf[0]	= reg_addr;
	buf[1]	= data_byte_1;
	buf[2]	= data_byte_2;
	msg.addr	= PCA9654_ADDR;			/*** SLAVE ADDRESS ***/
	msg.flags 	= 0;
	msg.len		= sizeof(buf);
	msg.buf		= buf;

	msgset.msgs	= &msg;
	msgset.nmsgs	= 1;

	result = ioctl(fd, I2C_RDWR, &msgset);			/*** I2C READ_WRITE ***/
	buf[1]  = 0x00;
	buf[2]  = 0x00;
	sleep (1);
	ioctl(fd, I2C_RDWR, &msgset);				/*** I2C READ_WRITE ***/
	
exit:
	/* Check return value of sequence */
	if (result == msgset.nmsgs) {
		printf ("OK\n");
	}
	else if (result == -1){
		printf ("NG\n");
	}
	else
		printf ("Unknown_result\n");
	close(fd);						/*** I2C CLOSE ***/
	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}

